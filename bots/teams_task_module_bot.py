# Copyright (c) Microsoft Corp. All rights reserved.
# Licensed under the MIT License.

import json
import os

import random

from botbuilder.core import (
    CardFactory,
    MessageFactory,
    TurnContext,
)
from botbuilder.schema import HeroCard, Attachment, CardAction, ChannelAccount, AttachmentLayoutTypes, Activity, ActivityTypes
from botbuilder.schema.teams import (
    TaskModuleMessageResponse,
    TaskModuleRequest,
    TaskModuleResponse,
    TaskModuleTaskInfo,
)
from botbuilder.core.teams import TeamsActivityHandler

from config import DefaultConfig
from models import (
    UISettings,
    TaskModuleUIConstants,
    TaskModuleIds,
    TaskModuleResponseFactory,
)
import psycopg2

class TeamsTaskModuleBot(TeamsActivityHandler):
    def __init__(self, config: DefaultConfig):
        self.__base_url = config.BASE_URL

    async def on_message_activity(self, turn_context: TurnContext):
        """
        This displays two cards: A HeroCard and an AdaptiveCard.  Both have the same
        options.  When any of the options are selected, `on_teams_task_module_fecth`
        is called.
        """
        text = turn_context.activity.text.strip().lower()
        if "vote" in text:
            reply = MessageFactory.list([])
            reply.attachment_layout = AttachmentLayoutTypes.carousel
            reply.attachments.append(self.__get_task_module_adaptive_card_options1())
            reply.attachments.append(self.__get_task_module_adaptive_card_options2())
            reply.attachments.append(self.__get_task_module_adaptive_card_options3())
            await turn_context.send_activity(reply)
        
        if "bot" in text:
            reply = MessageFactory.list([])
            reply.attachments.append(self.__get_task_module_hero_card_options())
            await turn_context.send_activity(reply)

    async def on_members_added_activity(
        self, members_added: [ChannelAccount], turn_context: TurnContext
    ):
        for member in members_added:
            if member.id != turn_context.activity.recipient.id:
                await turn_context.send_activity(
                    "Hello👋 My name is Eillot! A robot that can help you improve work "
                    f"efficiency and teamwork."
                    f"Next, I will launch the following 3️⃣ functions."
                    f"Which one do you like better? Please vote!✅"
                )

    async def on_teams_task_module_fetch(
        self, turn_context: TurnContext, task_module_request: TaskModuleRequest
    ) -> TaskModuleResponse:
       
        card_task_fetch_value = task_module_request.data["data"]

        task_info = TaskModuleTaskInfo()
        if card_task_fetch_value == TaskModuleIds.YOUTUBE:
            # Connect DataBase
            #add data to local postgresql database.
            conn = psycopg2.connect(database="test_cz",user="postgres",password="P@ssw0rd", host="127.0.0.1", port="5432")
            cur = conn.cursor()
            # first connect
            # cur.execute("CREATE TABLE voteResultRecord(id integer, name varchar, vote varchar,user_id varchar)")

            # insert data
            cur.execute("INSERT INTO  voteResultRecord(name, vote )VALUES(%s,%s)",(turn_context.activity.from_property.name,random.choice(["Community Assistant","Project Management Assistant","Help Center Assistant"])))

            # update data
            #cur.execute("UPDATE voteResultRecord SET id = %s , name = %s, vote = %s where user_id = %s",(3,"chenzhen","help assistant center","cmkr066542_uide5e48v2"))
            conn.commit()
            #print("已经创建数据库完毕")
            cur.close()
            conn.close()



            # Display the YouTube.html page
            task_info.url = task_info.fallback_url = (
                  self.__base_url + "/VotingResult.html"
            )

            TeamsTaskModuleBot.__set_task_info(task_info, TaskModuleUIConstants.YOUTUBE)

            return TaskModuleResponseFactory.to_task_module_response(task_info)

        elif card_task_fetch_value == TaskModuleIds.CUSTOM_FORM:
            # Display the CustomForm.html page, and post the form data back via
            # on_teams_task_module_submit.
            # task_info.url = task_info.fallback_url = (
            #     self.__base_url + "/" + TaskModuleIds.CUSTOM_FORM + ".html"
            # )
            task_info.url = task_info.fallback_url = ("https://www.youtube.com/watch?v=TE0cqdZVgUU")
            TeamsTaskModuleBot.__set_task_info(
                task_info, TaskModuleUIConstants.CUSTOM_FORM
            )
            return TaskModuleResponseFactory.to_task_module_response(task_info)
        elif card_task_fetch_value == TaskModuleIds.ADAPTIVE_CARD:
            # Connect DataBase
            #add data to local postgresql database.
            conn = psycopg2.connect(database="test_cz",user="postgres",password="P@ssw0rd", host="127.0.0.1", port="5432")
            cur = conn.cursor()
            # first connect 
            # cur.execute("CREATE TABLE voteResultRecord(id integer, name varchar, vote varchar,user_id varchar)")
            
            # insert data
            cur.execute("INSERT INTO  voteResultRecord(name, vote )VALUES(%s,%s)",(turn_context.activity.from_property.name,random.choice(["Community Assistant","Project Management Assistant","Help Center Assistant"])))

            # update data
            #cur.execute("UPDATE voteResultRecord SET id = %s , name = %s, vote = %s where user_id = %s",(3,"chenzhen","help assistant center","cmkr066542_uide5e48v2"))
            conn.commit()
            #print("已经创建数据库完毕")
            cur.close()
            conn.close()

            task_info.url = task_info.fallback_url = ("https://www.youtube.com/watch?v=TE0cqdZVgUU")
            TeamsTaskModuleBot.__set_task_info(
                task_info, TaskModuleUIConstants.ADAPTIVE_CARD
            )
            return TaskModuleResponseFactory.to_task_module_response(task_info)

            
            # on_teams_task_module_submit.
            #task_info.card = TeamsTaskModuleBot.__create_adaptive_card_attachment()
            #TeamsTaskModuleBot.__set_task_info(
            #    task_info, TaskModuleUIConstants.ADAPTIVE_CARD
            #)
            #return TaskModuleResponseFactory.to_task_module_response(task_info)

    async def on_teams_task_module_submit(
        self, turn_context: TurnContext, task_module_request: TaskModuleRequest
    ) -> TaskModuleResponse:
        """
        Called when data is being returned from the selected option (see `on_teams_task_module_fetch').
        """

        # Echo the users input back.  In a production bot, this is where you'd add behavior in
        # response to the input.
        await turn_context.send_activity(
            MessageFactory.text(
                f"on_teams_task_module_submit: {json.dumps(task_module_request.data)}"
            )
        )

        message_response = TaskModuleMessageResponse(value="Thanks!")
        return TaskModuleResponse(task=message_response)

    @staticmethod
    def _create_adaptive_card_attachment(self, num:int) -> Attachment:

        card_path = os.path.join(os.getcwd(), CARDS[num])
        with open(card_path, "rb") as in_file:
            card_data = json.load(in_file)
        return CardFactory.adaptive_card(card_data)

    @staticmethod
    def __set_task_info(task_info: TaskModuleTaskInfo, ui_constants: UISettings):
        task_info.height = ui_constants.height
        task_info.width = ui_constants.width
        task_info.title = ui_constants.title

    @staticmethod
    def __get_task_module_hero_card_options() -> Attachment:
        buttons = [
            CardAction(
                type="invoke",
                title=card_type.button_title,
                value=json.dumps({"type": "task/fetch", "data": card_type.id}),
            )
            for card_type in [
                # TaskModuleUIConstants.ADAPTIVE_CARD,
                # TaskModuleUIConstants.CUSTOM_FORM,
                TaskModuleUIConstants.YOUTUBE,
            ]
        ]

        card = HeroCard(title="Task Module Invocation from Hero Card", buttons=buttons)
        return CardFactory.hero_card(card)

    @staticmethod
    def __get_task_module_adaptive_card_options1() -> Attachment:
        adaptive_card = {
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "version": "1.0",
            "type": "AdaptiveCard",
            "body": [
                {
                    "type": "TextBlock",
                    "size": "default",
                    "weight": "Bolder",
                    "text": "Community Assistant"
                },
                {
                    "type": "TextBlock",
                    "text": "Foster collaboration,and bing people together wherever in the world.",
                    "wrap": True
                    
                },
                {
                    "type": "Image",
                    "url": "https://gitee.com/chenzhen444/bot-pic/raw/master/Sample1.png"
                },
                {
                    "type": "TextBlock",
                    "text": "Are you still worrying about lunch reservations🍛? Want to know how satisfied employees are with free coffee☕️? ",
                    "wrap": True
                },
                {
                    "type": "TextBlock",
                    "text": "Then you found a treasure!!! ",
                    "wrap": True
                },
                {
                    "type": "TextBlock",
                    "text": "You can plan📝 any formal or informal events with me, and I will take care of the rest.👌 ",
                    "wrap": True
                },
            ],
            "actions": [
                {
                    "type": "Action.Submit",
                    "title": card_type.button_title,
                    "data": {"msteams": {"type": "task/fetch"}, "data": card_type.id},
                }
                for card_type in [
                    TaskModuleUIConstants.YOUTUBE
                ]
            ],
        }

        return CardFactory.adaptive_card(adaptive_card)
    @staticmethod
    def __get_task_module_adaptive_card_options2() -> Attachment:
        adaptive_card = {
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "version": "1.0",
            "type": "AdaptiveCard",
            "body": [
                {
                    "type": "TextBlock",
                    "size": "default",
                    "weight": "Bolder",
                    "text": "Project Management Assistant"
                },
                {
                    "type": "TextBlock",
                    "text": "Stop wasting time in management,start making business decisions with Peter.",
                    "wrap": True
                },
                {
                    "type": "Image",
                    "url": "https://gitee.com/chenzhen444/bot-pic/raw/master/Sample2.png"
                },
                {
                    "type": "TextBlock",
                    "text": "Does your team have to collect daily reports manually📃?",
                    "wrap": True
                },
                {
                    "type": "TextBlock",
                    "text": " Are you still spending time organizing project data into charts📊? Leave it all to Peter!!! ",
                    "wrap": True
                },
                {
                    "type": "TextBlock",
                    "text": "You don’t need to worry about others but just focus on business decision-making.💼",
                    "wrap": True
                }
            ],
            "actions": [
                {
                    "type": "Action.Submit",
                    "title":  card_type.button_title,
                    "data": {"msteams": {"type": "task/fetch"}, "data": card_type.id},
                }
                for card_type in [
                    TaskModuleUIConstants.YOUTUBE
                ]
            ],
        }

        return CardFactory.adaptive_card(adaptive_card)
    @staticmethod
    def __get_task_module_adaptive_card_options3() -> Attachment:
        adaptive_card = {
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "version": "1.0",
            "type": "AdaptiveCard",
            "body": [
                {
                    "type": "TextBlock",
                    "size": "default",
                    "weight": "Bolder",
                    "text": "Help Center Assistant"
                },
                {
                    "type": "TextBlock",
                    "text": "Be a knowledgeable friend and be your side all the time.",
                    "wrap": True
                },
                {
                    "type": "Image",
                    "url": "https://gitee.com/chenzhen444/bot-pic/raw/master/Sample3.png"
                },
                {
                    "type": "TextBlock",
                    "text": "What should you do when you encounter problems at work🗯? ",
                    "wrap": True
                },
                {
                    "type": "TextBlock",
                    "text": "Do you find it troublesome when you need to jump between work software and search engines😕?Just ask Peter!!!  ",
                    "wrap": True
                },
                {
                    "type": "TextBlock",
                    "text": "You only need to enter your question in the chatbox and Peter will tell you everything he knows.🔎 ",
                    "wrap": True
                }
            ],
            "actions": [
                {
                    "type": "Action.Submit",
                    "title":  card_type.button_title,
                    "data": {"msteams": {"type": "task/fetch"}, "data": card_type.id},
                }
                for card_type in [
                    TaskModuleUIConstants.YOUTUBE
                ]
            ],
        }

        return CardFactory.adaptive_card(adaptive_card)

    @staticmethod
    def __create_adaptive_card_attachment() -> Attachment:
        card_path = os.path.join(os.getcwd(), "resources/adaptiveCard.json")
        with open(card_path, "rb") as in_file:
            card_data = json.load(in_file)

        return CardFactory.adaptive_card(card_data)
