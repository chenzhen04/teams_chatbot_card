#!/usr/bin/env python3
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

import os


class DefaultConfig:
    """ Bot Configuration """

    PORT = 7000
    APP_ID = os.environ.get("MicrosoftAppId", "8a2519d6-2b89-4222-84f2-59170cdf0b66")
    APP_PASSWORD = os.environ.get("MicrosoftAppPassword", "xFm~Z-.-Lxl_S~AVG38aSwN3hTq3EbJM3G")
    BASE_URL = os.environ.get("BaseUrl", "https://chatbotexcellence.bosch.com.cn/botframeworktom")
