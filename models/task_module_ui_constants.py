# Copyright (c) Microsoft Corp. All rights reserved.
# Licensed under the MIT License.

from .ui_settings import UISettings
from .task_module_ids import TaskModuleIds


class TaskModuleUIConstants:
    YOUTUBE = UISettings(
            600, 361, "Voting Result", TaskModuleIds.YOUTUBE, "vote",
            )
    CUSTOM_FORM = UISettings(
        510, 450, "Custom Form", TaskModuleIds.CUSTOM_FORM, "Custom Form"
    )
    ADAPTIVE_CARD = UISettings(
        600, 361, "Voting Result", TaskModuleIds.ADAPTIVE_CARD, "vote",
    )
